package shapes;

public class Square extends Shape{

    double edge;

    public Square(double edge) {
        this.edge = edge;
    }

    public double area(){
        return (edge * edge);
    }
}
