package drawing.version2;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;

import java.util.ArrayList;

public class Drawing {

	private ArrayList shapes = new ArrayList();
	
	public double calculateTotalArea(){
		double totalArea = 0;

		for (Object shape : shapes){
			if (shape instanceof Circle){
				Circle circle = (Circle) shape;
				totalArea += circle.area();
			}
			else if(shape instanceof Rectangle){
				Rectangle rect = (Rectangle) shape;
				totalArea += rect.area();
			}
			else if(shape instanceof Square){
				Square sqr = (Square) shape;
				totalArea += sqr.area();
			}
		}

		return totalArea;
	}

	public void addShape(Object shape){
		shapes.add(shape);
	}
}
