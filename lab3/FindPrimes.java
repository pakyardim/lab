public class FindPrimes {


    public static void main(String[] args){

        int number = Integer.parseInt(args[0]);

        if (number<2)
            System.out.println("You must type a number greater than 1!");

        for (int i = 2; i<number; i++) {
            int divisor = 2;
            boolean isPrime = true;

            while(divisor<i && isPrime){
                if(i % divisor == 0)
                    isPrime = false;
                divisor++;
            }
            if (isPrime)
                System.out.println(i + " ");
        }
    }
}