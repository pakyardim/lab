public class MyTime {

    int hour, minute;
    private MyDate date;

    public MyTime(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public String toString(){
        return (hour < 10 ? "0" : "")+hour+ ":" + (minute < 10 ? "0" : "") + minute;
    }

    public boolean isBefore(MyTime anotherTime) {
        int a = Integer.parseInt(toString().replaceAll(":", ""));

        int b = Integer.parseInt(anotherTime.toString().replaceAll(":", ""));

        return a < b;
    }

    public boolean isAfter(MyTime anotherTime) {
        int a = Integer.parseInt(toString().replaceAll(":", ""));
        int b = Integer.parseInt(anotherTime.toString().replaceAll(":", ""));
        return a > b;
    }

    public void incrementMinute(int minuteToIncrement){

        while (minuteToIncrement > 0){
            if (minute+1 == 60) {
                minute = 0;
                incrementHour();
            }
            else
                minute++;
            minuteToIncrement--;
        }
    }
    public void incrementHour(){
        if (hour+1 == 24) {
            hour = 0;
            date.incrementDay();
        }
        else
            hour++;
    }
    public void decrementHour(int hourToDecrement){

        while (hourToDecrement>0){
            int newHour = hour-1;

            if(newHour == -1){
                hour = 23;
                date.decrementDay();
            }
            else
                hour = newHour;
            hourToDecrement--;
        }

    }

    public void incrementHour(int hourToIncrement){
        while (hourToIncrement > 0){
            incrementHour();
            hourToIncrement--;
        }
    }

    public void decrementMinute(int minuteToDecrement) {

        while (minuteToDecrement > 0) {
            int newMinute = minute - 1;
            if (newMinute == -1) {
                minute = 59;
                decrementHour(1);
            } else
                minute--;
            minuteToDecrement--;
        }
    }

    public int timeDifference(MyTime anotherTime) {
        int diff = 0;
        if (isBefore(anotherTime)){
            MyTime time = new MyTime(hour,minute); //a copy of current object
            while (time.isBefore(anotherTime)){
                time.incrementMinute(1);
                diff++;
            }
        }
        else if(isAfter(anotherTime)){
            MyTime time = new MyTime(hour,minute); // a copy of current object
            while (time.isAfter(anotherTime)){
                time.decrementMinute(1);
                diff++;
            }
        }
        return diff;
    }
}
