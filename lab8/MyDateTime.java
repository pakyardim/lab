public class MyDateTime {

    private MyDate date;
    private MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time = time;
    }

    public boolean isBefore(MyDateTime anotherDateTime){

        if (date.isBefore(anotherDateTime.date))
            return true;
        else if(date.isAfter(anotherDateTime.date))
            return false;
        else {
            if (time.isBefore(anotherDateTime.time))
                return true;
            else
                return false;
        }
    }

    public boolean isAfter(MyDateTime anotherDateTime) {
        if (date.isBefore(anotherDateTime.date))
            return false;
        else if(date.isAfter(anotherDateTime.date))
            return true;
        else {
            if (time.isBefore(anotherDateTime.time))
                return false;
            else
                return true;
        }
    }

    public String dayTimeDifference(MyDateTime anotherDateTime) {
        int dayDiff = date.dayDifference(anotherDateTime.date);
        int timeDiff = time.timeDifference(anotherDateTime.time);


    }

    public String toString(){
        return date.toString() + " " + time.toString();
    }

    public void incrementDay(){
        date.incrementDay();
    }

    public void incrementHour(){
        time.incrementHour();
    }

    public void incrementHour(int hourToIncrement){
        time.incrementHour(hourToIncrement);
    }

    public void decrementHour(int hourToDecrement){
        time.decrementHour(hourToDecrement);
    }

    public void incrementMinute(int minuteToIncrement){
        time.incrementMinute(minuteToIncrement);
    }

    public void decrementMinute(int minuteToDecrement) {
        time.decrementMinute(minuteToDecrement);
    }

    public void incrementYear(){
        date.incrementYear();
    }
    public void incrementYear(int year){
        date.incrementYear(year);
    }

    public void decrementDay(){
        date.decrementDay();
    }

    public void decrementYear(){
        date.decrementYear();
    }
    public void decrementYear(int year){
        date.decrementYear(year);
    }

    public void incrementMonth(){
        date.incrementMonth();
    }

    public void incrementMonth(int month){
        date.incrementMonth(month);
    }

    public void decrementMonth(){
        date.decrementMonth();
    }

    public void decrementMonth(int month){
        date.decrementMonth(month);
    }

    public void incrementDay(int day){
        date.incrementDay(day);
    }

    public void decrementDay(int day){
        date.decrementDay(day);
    }



}

