public class Main {

    public static void main(String[] args) {
        Rectangle rect =new Rectangle(10,12, new Point(3,2));
        System.out.println("Area of rectangle is: "+rect.area());
        System.out.println("Perimeter of rectangle is: "+rect.perimeter());
        Point[] corners =rect.corners();
        for(int i=0;i<corners.length;i++){
            Point p=corners[i];
            if (p==null)
                System.out.println("p is null");
            else{
                System.out.println("corner  "+(i+1)+" x = "+p.getxCoord()+" y = "+p.getyCoord());
            }
        }

        Circle c=new Circle(8,new Point (15,12));
        System.out.println("Area of the circle is: "+c.area());
        System.out.println("Perimeter of the circle is: "+c.perimeter());

        Circle c2 = new Circle(5,new Point(22,22));
        System.out.println(c.intersect(c2));
        System.out.println(c.intersect(c));



    }
}
